CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainer


INTRODUCTION
------------

Custom node export module intends to export nodes which helps to data migration.
Run command: drush nw, Node data(title, nid, created date, type and url)
of all content types will be written into xls file.


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

No recommended module is needed.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.

MAINTAINERS
-----------

Current maintainers:
 * sunil-singh -https://www.drupal.org/u/sunil-singh
