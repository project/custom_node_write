<?php

/**
 * Implements hook_drush_command().
 */
function custom_node_write_drush_command() {
  $items['node-content'] = array(
      'description' => 'node details write in file.',
      'callback' => 'drush_custom_node_content',
      'aliases' => array('nw'),
      'examples' => array(// List these example when user types : drush help nc
          'Get all nodes count' => 'drush nw',
          'Get articles node count' => 'drush nw article',
          'Get articles, pages node count' => 'drush nw article page',
      ),
  );
  return $items;
}

/*
 * Callback function for hook_drush_command().
 */

function drush_custom_node_content() {
  $file = drupal_get_path('module', 'custom_node_write') . '/node_content.xls';
  // Open the file to get existing content
  $current = file_get_contents($file);

  global $base_url;
  $article_url = "";
  $article_url_alias = "";
  $nids = "";

  $args = func_get_args();
  if ($args) {
    foreach ($args as $type) {
      $query = db_select('node', 'n');
      $result = $query
              ->fields('n', array('nid', 'title', 'type', 'created'))
              ->condition('type', $type, '=')
              ->orderBy('n.nid', 'DESC')
              ->execute();
      $Node_Data = "";

      foreach ($result as $node_rc) {
        $nids = $node_rc->nid;
        $title = $node_rc->title;
        $content_type = $node_rc->type;
        $create_date = date('d M Y H:i:s', $node_rc->created);
        $article_url_alias = drupal_lookup_path('alias', "node/" . $nids);
        $article_url = $base_url . '/' . $article_url_alias;
        if ($nids !== "") {
          $Node_Data = $nids . "\t" . $title . "\t" . $content_type . "\t" . $create_date . "\t" . $article_url . "\n";
          $current .= $Node_Data;
        } else {
          
        }
      }

      $header = "Article ID" . "\t";
      $header .= "Title" . "\t";
      $header .= "Node Type" . "\t";
      $header .= "Create Date" . "\t";
      $header .= "Article Url" . "\t";
      file_put_contents($file, $header . $current);
    }
  }
}